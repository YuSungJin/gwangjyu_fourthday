﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
	[SerializeField]private GameObject		particle_prefab;
	[SerializeField]private	TileManager		tileManager;
	[SerializeField]private	CameraManager	cameraManager;
	public	bool	IsOn_Magnet { set; get; }

	void Awake()
	{
		IsOn_Magnet = false;
		Invoke("Create_Particle", 3.0f);
	}
	void Update()
	{
		if ( Input.GetKeyDown(KeyCode.Escape) )
			Application.Quit();

#if UNITY_EDITOR
		if ( Input.GetKeyDown(KeyCode.Space) )
			cameraManager.Is_Run = !cameraManager.Is_Run;
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
		if ( IsOn_Magnet )
		{
			cameraManager.Is_Run = !cameraManager.Is_Run;
			IsOn_Magnet = false;
		}
#endif
	}
	public void Create_Particle()
	{
		GameObject clone = Instantiate(particle_prefab) as GameObject;
		clone.name = "Particle";

		Tile tile;
		do {
			tile = tileManager.Get_Tile(Random.Range(0, Constants.width * Constants.height));
		} while ( tile.Type != TILE_TYPE.ROAD );

		clone.transform.position = new Vector3(tile.Get_Pos().x, .5f, tile.Get_Pos().z);
	}
}

