﻿using UnityEngine;
using System.Collections;

public class Tile : BaseObject
{
	[SerializeField]private	Texture2D[]	tex;

	private	Renderer	_render;
	private	TILE_TYPE	type;

	public void Init(TILE_TYPE _type, Vector3 _pos)
	{
		base.Init();

		Set_Pos(_pos);

		_render = GetComponent<Renderer>();
		Type	= _type;
	}
	public TILE_TYPE Type
	{
		get { return type; }
		set
		{
			type = value;
			_render.material.mainTexture		= tex[(int)type];
			_render.material.mainTextureScale	= new Vector2(2.0f, 2.0f);
			switch ( type )
			{
				case TILE_TYPE.WALL:
					transform.localScale = new Vector3(2.0f, 2.0f, 2.0f);
					Set_Pos(new Vector3(Get_Pos().x, 0.95f, Get_Pos().z));
					break;
				case TILE_TYPE.ROAD:
				case TILE_TYPE.WATER:
					transform.localScale = new Vector3(2.0f, .1f, 2.0f);
					break;
			}
		}
	}
}

