﻿using UnityEngine;
using System.Collections;

public class BaseObject : MonoBehaviour
{
	protected	Vector3	pos;
	protected	float	speed;

	public void Init()
	{
		Set_Pos(Vector3.zero);
		Set_Speed(.0f);
	}
	public	void		Add_Pos(Vector3 value)	{ Set_Pos(Get_Pos()+value); }
	public	void		Set_Pos(Vector3 value)	{ pos = value; transform.position = pos; }
	public	Vector3		Get_Pos()				{ return pos; }

	public	void		Set_Speed(float value)	{ speed = value; }
	public	float		Get_Speed()				{ return speed; }
}

