﻿using UnityEngine;
using System.Collections;

public class CameraManager : BaseObject
{
	[SerializeField]private	Vector3		base_pos;
	[SerializeField]private GameObject	head;
	[SerializeField]private	GameManager	gameManager;
	private	RaycastHit	hit;
	public	bool		Is_Run { get; set; }

	void Awake()
	{
		base.Init();

		Set_Pos(base_pos);
		Set_Speed(3.0f);

		Is_Run = false;
	}
	void Update()
	{
		if ( !Is_Run ) return;

		Vector3 pos = head.transform.TransformDirection(Vector3.forward);
		if ( Physics.Raycast(Get_Pos(), pos, out hit, 1.0f) )
		{
			if ( hit.transform.name.Equals("Tile") )
				return;
		}
		pos.y = .0f;
		Add_Pos(pos*Get_Speed()*Time.deltaTime);
	}
	void OnTriggerEnter(Collider col)
	{
		if ( col.name.Equals("Particle") )
		{
			Destroy(col.gameObject);
			gameManager.Create_Particle();
		}
	}
}

