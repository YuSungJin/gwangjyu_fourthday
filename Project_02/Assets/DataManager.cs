﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class DataManager : MonoBehaviour
{
	[SerializeField]private TileManager tileManager;

	void Awake()
	{
#if UNITY_EDITOR
		Load_Map_Data_Editor("RandomMaze.txt");
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
		StartCoroutine(Load_Map_Data_Android("RandomMaze.txt"));
#endif
	}
#if UNITY_EDITOR
	public void Load_Map_Data_Editor(string file_name)
	{
		string _file_name = Path.Combine(Application.streamingAssetsPath, file_name);
		StreamReader sr = new StreamReader(_file_name);
		
		string		str_map_size	= sr.ReadLine();
		string[]	size_arr		= str_map_size.Split('\t');
		int			map_x			= int.Parse(size_arr[1]);
		int			map_y			= int.Parse(size_arr[2]);

		string		str_map_data	= sr.ReadToEnd();
		string[]	str_arr			= str_map_data.Split('\t');
		Constants.map_data			= new List<int>(str_arr.Length-1);
		for ( int i = 0; i < (int)str_arr.Length-1; ++ i )
		{
			string str = str_arr[i];
			Constants.map_data.Add(int.Parse(str));
		}

		Constants.width  = map_x;
		Constants.height = map_y;

		sr.Close();

		tileManager.Create_All_Tiles_In_File(Constants.width, Constants.height, Constants.map_data);
	}
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
	IEnumerator Load_Map_Data_Android(string file_name)
	{
		var path = Path.Combine(Application.streamingAssetsPath, file_name);
		WWW www	 = new WWW(path);
		yield return www;

		string[] str_data = www.text.Split('/');

		string str_map_size		= str_data[0];
		string[]	size_arr	= str_map_size.Split('\t');
		int			map_x		= int.Parse(size_arr[1]);
		int			map_y		= int.Parse(size_arr[2]);

		string str_map_data = str_data[1];
		string[]	str_arr			= str_map_data.Split('\t');
		Constants.map_data			= new List<int>(str_arr.Length-1);
		for ( int i = 0; i < (int)str_arr.Length-1; ++ i )
		{
			string str = str_arr[i];
			Constants.map_data.Add(int.Parse(str));
		}

		Constants.width  = map_x;
		Constants.height = map_y;

		tileManager.Create_All_Tiles_In_File(Constants.width, Constants.height, Constants.map_data);
	}
#endif
}

