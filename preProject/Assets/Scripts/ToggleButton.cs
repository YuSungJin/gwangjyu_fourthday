﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ToggleButton : MonoBehaviour {


    Toggle mToggle;

	// Use this for initialization
	void Start () {

        mToggle = GetComponent<Toggle>();


        if (mToggle.isOn == true)
            mToggle.isOn = false;

	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void PointerEnter()
    {

        mToggle.isOn =! mToggle.isOn;
        bool value = mToggle.isOn;

        ScoreManager.bUseMagnet = mToggle.isOn;

    }

}
