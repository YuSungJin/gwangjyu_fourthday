﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	static ScoreManager _instance; public static ScoreManager Instance{get{return _instance;}}
	public static int score = 0;

    public static bool bUseMagnet = false;
    public bool UseMagnet { get { return bUseMagnet; }  set { bUseMagnet = value; } }


    public Text textScore;

	void Awake()
	{
		_instance = this;
        CardboardMagnetSensor.SetEnabled(bUseMagnet);
	}

	void Start()
	{
		score = 0;
	}

	public void HitTarget()
	{
		score++;
		textScore.text = score.ToString ();
	}
}
