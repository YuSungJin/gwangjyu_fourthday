﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour {

	public float limit = 15f;
	public Image bar;

	void Update()
	{
		if (limit > 0f) {
			limit -= Time.deltaTime;
			bar.fillAmount = limit / 15f;
		} else {
			ScreenCtrl.ChangeScene ("FPS_Result");
//			Application.LoadLevel ("FPS_Result");
		}
	}
}
