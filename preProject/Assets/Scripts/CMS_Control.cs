﻿using UnityEngine;
using System.Collections;

public class CMS_Control : MonoBehaviour
{
	[SerializeField]
	private	GameObject box_prefab;

	public bool magnetDetectionEnabled = true;

	void Awake()
	{
		CardboardMagnetSensor.SetEnabled(magnetDetectionEnabled);
		// Disable screen dimming:
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}
	void Update()
	{
		if ( !magnetDetectionEnabled ) return;

		if ( CardboardMagnetSensor.CheckIfWasClicked() )
		{
			GameObject clone = Instantiate(box_prefab) as GameObject;
			
			float fx = Random.Range(-4.0f, 4.0f);
			float fz = Random.Range(-4.0f, 4.0f);
			clone.transform.position = new Vector3(fx, 3.0f, fz);
			clone.GetComponent<Renderer>().material.color = new Color(Random.Range(.0f, 1.0f),
									Random.Range(.0f, 1.0f), Random.Range(.0f, 1.0f), 1.0f);

			CardboardMagnetSensor.ResetClick();
		}
	}
}

