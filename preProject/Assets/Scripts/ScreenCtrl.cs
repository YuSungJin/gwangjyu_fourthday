﻿using UnityEngine;
using System.Collections;

public class ScreenCtrl : MonoBehaviour {

	static ScreenCtrl _instance; public static ScreenCtrl Instance{get{ return _instance; }}

	Renderer renderer;

	void Awake()
	{
		_instance = this;
	}

	void Start()
	{
		renderer = GetComponent<Renderer> ();

		StartCoroutine ("FadeOut");
	}

	IEnumerator FadeOut()
	{
		Color color = Color.black;

		while (true) {

			yield return null;

			color.a -= Time.deltaTime;

			renderer.material.SetColor("_Tint", color);

			if (color.a <= 0f)
				break;
		}
	}

	public static void ChangeScene(string scene)
	{
        //StartCoroutine ("FadeIn", scene);
        Application.LoadLevel(scene);
    }

	IEnumerator FadeIn(string scene)
	{
		Color color = Color.black;
		color.a = 0f;

		while (true) {

			yield return null;

			color.a += Time.deltaTime;

			renderer.material.SetColor("_Tint", color);

			if (color.a >= 1f)
				break;
		}

		Application.LoadLevel (scene);
	}
}
