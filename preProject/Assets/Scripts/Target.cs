﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

	public GameObject shatter;

	void Start()
	{
		transform.LookAt(Camera.main.transform);

		Destroy (gameObject, 2f);
	}


    private void OnDestroy()
    {
        CardboardMagnetSensor.ResetClick();

        GvrPointerManager.Pointer.OnPointerExit(this.gameObject);
    }

    public void OnGazeEnter()
	{
        
        if (ScoreManager.bUseMagnet)
        {
            if (CardboardMagnetSensor.CheckIfWasClicked())
            {

                Destroy(gameObject);
                GameObject obj = Instantiate(shatter);
                obj.transform.position = transform.position;

                ScoreManager.Instance.HitTarget();

                Destroy(obj, 5f);

                CardboardMagnetSensor.ResetClick();
            }

            else
                return;
        }

        else
        {

            Destroy(gameObject);
            GameObject obj = Instantiate(shatter);
            obj.transform.position = transform.position;

            ScoreManager.Instance.HitTarget();

            Destroy(obj, 5f);

        }


	}

	public void OnGazeExit()
	{
		
	}
}
