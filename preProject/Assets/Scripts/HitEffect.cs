﻿using UnityEngine;
using System.Collections;

public class HitEffect : MonoBehaviour {

	public GameObject fx;
	public LineRenderer line;
	Vector3 point;

	public void OnHitTarget(RaycastHit hit)
	{

        if (ScoreManager.bUseMagnet)
        {

            if (CardboardMagnetSensor.CheckIfWasClicked())
            {
                fx.SetActive(true);
                line.enabled = true;

                point = hit.point;

                Invoke("TurnOff", 0.2f);
            }
            else
                return;

        }
        else
        {
            fx.SetActive(true);
            line.enabled = true;

            point = hit.point;

            Invoke("TurnOff", 0.2f);
        }

	}

	void Update()
	{

        CardboardMagnetSensor.CheckIfWasClicked();

        if (line.enabled) {

			line.SetPosition (0, line.transform.position);
			line.SetPosition (1, point);
		}
	}

	void TurnOff()
	{
		fx.SetActive (false);
		line.enabled = false;
	}
}
