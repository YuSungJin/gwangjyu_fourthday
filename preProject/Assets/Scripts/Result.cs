﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Result : MonoBehaviour {

	public Text result;

	void Start()
	{
		result.text = "Your Score : " + ScoreManager.score;
	}

	public void OnGoToTitle()
	{
		ScreenCtrl.ChangeScene ("FPS_Title");
//		Application.LoadLevel ("FPS_Title");
	}
}
