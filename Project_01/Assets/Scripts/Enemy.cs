﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public GameObject _explosion;

    NavMeshAgent _agent;
	GameObject endPosition;

	void Start () {
		
		_agent = gameObject.GetComponent<NavMeshAgent> ();

		endPosition = GameObject.Find ("EndPos");

		_agent.destination = endPosition.transform.position;
	}

    public void OnGazeEnter()
    {
        Destroy(gameObject);

        GameObject ob = Instantiate(_explosion);
        ob.transform.position = transform.position;
    }
}
