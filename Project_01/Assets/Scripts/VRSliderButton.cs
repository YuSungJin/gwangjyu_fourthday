﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class VRSliderButton : MonoBehaviour {

	bool triggerEnter = false;
	float progress = 0f;
	public UnityEvent selectEvent;

	void Update()
	{
		if (triggerEnter) {

			progress = progress + Time.deltaTime;
			GetComponent<Slider> ().value = progress;

			if (progress >= 1f) {
				selectEvent.Invoke ();

                Destroy (gameObject);
			}
		}
	}

    //void OnTriggerEnter(Collider col)
    //{
    //    triggerEnter = true;
    //}

    //void OnTriggerExit(Collider col)
    //{
    //    triggerEnter = false;
    //    progress = 0f;
    //    GetComponent<Slider>().value = 0f;
    //}

    public void OnGazeEnter()
    {
        triggerEnter = true;
    }

    public void OnGazeExit()
    {
        triggerEnter = false;
        progress = 0f;
        GetComponent<Slider>().value = 0f;
    }
}
