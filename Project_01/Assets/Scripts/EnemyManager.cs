﻿using UnityEngine;
using System.Collections;
public class EnemyManager : MonoBehaviour
{
    // 생성할 enemy 객체 prefab 을 받을 변수
    public GameObject enemy;
    public Transform startPos;
    // 적을 생성할 시간
    public float MAKE_TIME = 2.0f;
    // 현재 지나간 시간을 저장할 변수
    float currentTime;    

    // Update is called once per frame
    void Update () {
        // 현재 시간이 지정된 MAKE_TIME 시간을 지날 경우 적을 생성	
        currentTime += Time.deltaTime;
        if (currentTime > MAKE_TIME)
        {
            currentTime = 0;

            // enemy 를 생성하여 시작위치에 배치를 한다..
            GameObject obj = Instantiate(enemy);
            obj.transform.position = startPos.position + Vector3.up * 0.5f;
        }
    }

}
