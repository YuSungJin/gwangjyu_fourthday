﻿using UnityEngine;
using System.Collections;

public class CameraPosition : MonoBehaviour {

    public GameObject main;
    public Transform[] pos = new Transform[4];
    public Transform nextPos;
    int index = 0;

    void Update()
    {
        if (GameOver.gameOver == false)
        {
            main.transform.position = Vector3.Lerp(main.transform.position, nextPos.position, Time.deltaTime);
            main.transform.eulerAngles = Vector3.Lerp(main.transform.eulerAngles, nextPos.eulerAngles, Time.deltaTime);

            if (Input.GetMouseButtonDown(1))
            {
                nextPos = pos[(index++) % 4];
            }
        }
    }

    void OnMagneticClicked()
    {
        nextPos = pos[(index++) % 4];
    }
}
