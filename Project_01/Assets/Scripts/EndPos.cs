﻿using UnityEngine;
using System.Collections;

public class EndPos : MonoBehaviour {

	public GameObject _button;

	void Start()
	{
		// 시작시 버튼을 비활성화 시킨다.
		_button.SetActive (false);
	}
	// 적과 최종 목적지가 충돌할 경우 Gameover 버튼을 활성화 시킨다.
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Enemy")
		{
			_button.SetActive (true);
		}
	}
}
