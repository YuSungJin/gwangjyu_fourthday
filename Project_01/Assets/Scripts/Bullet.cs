﻿using UnityEngine;
using System.Collections;
public class Bullet : MonoBehaviour
{
    // 물리 동작을 위한 Rigidbody 컴포넌트	
    Rigidbody rigid;
    // 총알의속도. 외부 노출을 위해 public 으로 선언	
    public float Speed = 100;
    public GameObject _explosion;

    // Use this for initialization	
    void Start () {
        // GameObject 에 붙어 있는 컴포넌트를 얻어 오기위해선 Getcomponent 함수를 이용		
        rigid = GetComponent<Rigidbody> ();
        // 전방방향의 속도를 지정		
        rigid.velocity = transform.forward * Speed;
    }

    void OnCollisionEnter(Collision other)
    {
        // 충돌한 대상의 tag 가 enemy 인지 여부 검사	
        if (other.gameObject.tag == "Enemy") {

            GameObject ob = Instantiate(_explosion);
            ob.transform.position = other.transform.position;
            // enemy 제거	
            Destroy (other.gameObject);
        }
        // Bullet 자신 제거	
        Destroy (gameObject);
    }
}
